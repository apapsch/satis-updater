<?php
/* Copyright (C) 2018 Aljosha Papsch <aljosha.papsch@vinexus.eu> */

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UpdateController
{

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string
     */
    private $satisBinary;
    /**
     * @var string
     */
    private $satisConfigFile;
    /**
     * @var string
     */
    private $satisWebDir;

    public function __construct(LoggerInterface $logger,
                                string $satisBinary,
                                string $satisConfigFile,
                                string $satisWebDir)
    {
        $this->logger = $logger;
        $this->satisBinary = $satisBinary;
        $this->satisConfigFile = $satisConfigFile;
        $this->satisWebDir = $satisWebDir;
    }

    public function onRepositoryUpdate(Request $request): JsonResponse
    {
        $payload = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \InvalidArgumentException('Payload could not be parsed: ' . json_last_error_msg());
        }
        $this->processBitbucketPayload($payload);
        return new JsonResponse([
            'message' => 'OK'
        ]);
    }

    private function processBitbucketPayload(array $payload): void
    {
        $commit = $payload['push']['changes'][0]['commits'][0];
        $repositoryName = $payload['repository']['full_name'];
        $repositoryUrl = 'git@bitbucket.org:' . $repositoryName . '.git';
        $satisCmd = sprintf('%s build --repository-url %s %s %s',
            $this->satisBinary,
            escapeshellarg($repositoryUrl),
            escapeshellarg($this->satisConfigFile),
            escapeshellarg($this->satisWebDir));
        $this->logger->notice('Invoking Satis on commit', [
            'repository_url' => $repositoryUrl,
            'commit_id' => $commit['hash'],
            'commit_message' => $commit['message'],
            'cmd' => $satisCmd
        ]);
        $output = [];
        $exitCode = -1;
        exec($satisCmd, $output, $exitCode);
        $this->logger->info('Satis finished', [
            'cmd' => $satisCmd,
            'exit_code' => $exitCode,
            'output' => $output
        ]);
    }

}